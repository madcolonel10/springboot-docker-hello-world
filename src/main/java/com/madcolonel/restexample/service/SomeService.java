package com.madcolonel.restexample.service;

import com.madcolonel.restexample.pojo.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class SomeService {
    public ApiResponse doStuff() {
        //do some processing here
        return new ApiResponse(HttpStatus.OK.value(), "this works");
    }
}
