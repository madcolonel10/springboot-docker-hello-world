package com.madcolonel.restexample.controller;

import com.madcolonel.restexample.pojo.ApiResponse;
import com.madcolonel.restexample.service.SomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class HomeController {

    SomeService service;

    @Autowired
    public HomeController(SomeService service) {
        this.service = service;
    }

    @GetMapping("")
    public String helloWorld() {
        return "v1 version of api";
    }

    @PostMapping(value = "", produces = "application/json", consumes = "application/json")
    public ApiResponse doStuff(@RequestBody Map<String, Object> payload) {
        System.out.println("json body" + payload);
        ApiResponse apiResponse = service.doStuff();
        apiResponse.setStatusCode(apiResponse.getStatusCode());
        return apiResponse;
    }
}
